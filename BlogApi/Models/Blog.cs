﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BlogApi.Models
{
    public class Blog
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Tiêu đề")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Mô tả ngắn")]
        [StringLength(200, MinimumLength = 3)]
        public string Summary { get; set; }

        [Display(Name = "Chi tiết")]
        [StringLength(2000, MinimumLength = 3)]
        public string Detail { get; set; }

        [Display(Name = "Vị trí")]
        public string Location { get; set; }

        [Display(Name = "Trạng thái")]
        public string Status { get; set; }

        [Display(Name = "Loại")]
        [Required]
        public string Category { get; set; }

        [Display(Name = "Ngày Public")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public string PublicDate { get; set; }
        
    }
}
