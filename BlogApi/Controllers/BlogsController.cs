﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BlogApi.Models;

namespace BlogApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : ControllerBase
    {
        private readonly BlogContext _context;

        public BlogsController(BlogContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all items (blogs)
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Blog>>> GetBlogs()
        {
            return await _context.Blogs.ToListAsync();
        }

        /// <summary>
        /// Get an item (blog) by Id
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Blog>> GetBlogById(int id)
        {
            var blog = await _context.Blogs.FindAsync(id);

            if (blog == null)
            {
                return NotFound();
            }

            return blog;
        }

        /// <summary>
        /// Get a list of items (blogs) by title
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpGet("{title}/search")]
        public async Task<ActionResult<IEnumerable<Blog>>> GetBlogByName(string title)
        {
            //var blog = await _context.Blogs.FindAsync(title);
            var blog = from m in _context.Blogs select m;

            if (!String.IsNullOrEmpty(title))
            {
                blog = blog.Where(s => s.Title.Contains(title));
            }

            if (blog == null)
            {
                return NotFound();
            }

            return await blog.ToListAsync();
        }

        /// <summary>
        /// Update an existing item (blog) by id
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBlog(int id, Blog blog)
        {
            if (id != blog.Id)
            {
                return BadRequest();
            }

            _context.Entry(blog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new item (blog)
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Blog>> PostBlog(Blog blog)
        {
            _context.Blogs.Add(blog);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBlogById), new { id = blog.Id }, blog);
        }

        /// <summary>
        /// Delete an item (blog)
        /// Created by :NamVTP (6/5/2022)
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            var blog = await _context.Blogs.FindAsync(id);
            if (blog == null)
            {
                return NotFound();
            }

            _context.Blogs.Remove(blog);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BlogExists(int id)
        {
            return _context.Blogs.Any(e => e.Id == id);
        }
    }
}
